"""
Uses the sas7bdat library to convert .sas7bdat files to csvs via pandas

"""

# Imports ----------------------------------------------------------------------
from sas7bdat import SAS7BDAT
import pandas as pd
import time
import argparse

# Input ------------------------------------------------------------------------

# Setup the arg parse
parser = argparse.ArgumentParser(description="Converts a '.sas7bdat' "
    + " into a '.csv'")
parser.add_argument("input_file", 
    help="File path of input '.sas7dbat' file.")
parser.add_argument("--output_file",
    help="Output file path. Defaults to './output.csv'.",
    default="./output.csv")
parser.add_argument("--verbose", 
    action="store_true",
    help="Increase output verbosity.")

def main():
    """ Main program.

    Parses arguements, performs the copy
    """

    # parse the args
    args = parser.parse_args()
    
    # time the operation - if verbose
    if args.verbose:
        start = time.time()

    # open the file
    with SAS7BDAT(args.input_file) as reader:
        df = reader.to_data_frame()  # save it as a pandas dataframe

    # print some stats on the file - if verbose
    if args.verbose:
        print(df.dtypes)
        print(df)

    # save the data frame to a csv
    df.to_csv(args.output_file)

    # report completion and time - if verbose
    if args.verbose:
        print("DONE - completed in " + str(time.time()-start) + " seconds")


if __name__ == '__main__':
    main()