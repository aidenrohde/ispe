NHTSA ISPE
===

This project uses NHTSA FARS data and Google Streetview to determine the practical effectiveness of guardrail terminals
across the nation. Our goal is to conduct an in-service performance evaluation (ISPE) on a range of terminals, barriers,
and crash cushions.

Data Sources
---

The National Highway Traffic Safety Administration (NHTSA), publishes a number of valuable data sets for studying the
performance of roadside safety hardware. These include:
- The Fatal Accident Reporting System (FARS) [info](https://www.nhtsa.gov/research-data/fatality-analysis-reporting-system-fars) [download](https://www.nhtsa.gov/node/97996/251)
    + GPS locations of each crash are given in the `accident.sas7bdat` file in the `FARSXXXXNationalSAS` folder
- The Crash Report Sampling System (CRSS) [info](https://www.nhtsa.gov/crash-data-systems/crash-report-sampling-system) [download](https://www.nhtsa.gov/node/97996/221) 

Given the GPS coordinates of a crash one can assess that crash using a variety of tools:
- Google Streetview - paste the coordinates into the search bar on the [maps site](https://www.google.com/maps/) and try using the [time travel feature](https://blog.google/products/maps/go-back-in-time-with-street-view/)
- Mapillary - paste the coordinates in the search bar within the [app](https://www.mapillary.com/app/)


Scripts
---

### sas7bdat_to_csv.py
This python script takes the .sas7bdat files that NHTSA provides and turns them into CSVs for simpler manipulation.


Setup
---

The following simplified setup instructions are provided for Linux. user on other operating systems will need to adjust
to suit. Mileage may vary. You'll need Python 3, Git, Pip and Venv installed to get started. 

1. Clone the script from gitlab `git clone git@gitlab.com:54f37y/ispe.git`
2. Navigate to the project directory `cd ./ispe`
3. Create a virtual environment `python3 -m venv venv`
4. Launch the venv `source ./venv/bin/activate`
5. Install the python dependencies `pip install -r requirements.txt`
6. Check the script using `python3 sas7bdat_to_csv.py -h`

With luck, you should see a helpful help menu, and the script should be ready to go. 
